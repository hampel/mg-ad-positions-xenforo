CHANGELOG
=========

1.1.0 (2020-11-05)
------------------

* added new position - XFMG Album view: Below album info block

1.0.0 (2020-10-30)
------------------

* initial working version - XFMG Media view: Below media info block
