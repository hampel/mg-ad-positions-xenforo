Media Gallery Advertisement Positions for XenForo 2.x
=====================================================

This XenForo 2.x addon adds new advertisement positions to the media gallery addon.

More information here: [Media Gallery Advertisement Positions](https://xenforo.com/community/resources/media-gallery-advertisement-positions.8105/)
(XenForo Addon Resource)

By [Simon Hampel](https://twitter.com/SimonHampel).
